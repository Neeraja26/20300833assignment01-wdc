var express = require('express');
app = express();
var router = express.Router();

PORT= 4548;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

app.get('/', function(req, res){ 
	app.use(express.static('public'))
    res.redirect('index.html'); 
});

app.listen(PORT, function(err){ 
    if (err) console.log(err); 
    console.log("Server listening on PORT", PORT); 
}); 

module.exports = router;
